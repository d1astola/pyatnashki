//
//  GameService.h
//  Pyatnashki
//
//  Created by Tester on 3/14/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    difficultyGameTypeEasy,
    difficultyGameTypeMedium,
    difficultyGameTypeHard
} difficultyGameType;

@protocol GameControllerDelegate;

@interface GameService : NSObject

/* delegate */
@property (weak, nonatomic) id <GameControllerDelegate> delegate;

/* public properties */
@property (assign, nonatomic) difficultyGameType difficultGameCurrent;
@property (weak, nonatomic) UILabel *counterLabel;
@property (weak, nonatomic) UILabel *timerLabel;
@property (strong, nonatomic) NSArray* buttonsArray;
@property (strong, nonatomic) NSArray* availableButtonsIndex;

/* availiable buttons specially for swipes gestures */
@property (weak, nonatomic) UIButton* leftSideButton;
@property (weak, nonatomic) UIButton* rightSideButton;
@property (weak, nonatomic) UIButton* upSideButton;
@property (weak, nonatomic) UIButton* downSideButton;

- (void) generateGamefield;
- (void) moveButton: (UIButton*) sender;

@end

@protocol GameControllerDelegate

@required
- (void) alert: (UIAlertController*) alertController;
- (void) addToView: (UIView*) view;

@end
