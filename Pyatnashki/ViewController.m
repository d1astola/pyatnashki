//
//  ViewController.m
//  Pyatnashki
//
//  Created by Tester on 3/13/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) GameService* gm;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.gm = [[GameService alloc] init];
    self.gm.delegate = self;
    self.gm.counterLabel = self.counterLabel;
    self.gm.timerLabel = self.timerLabel;
    
    self.gm.buttonsArray = [self.buttonSuperview subviews];
    self.gm.difficultGameCurrent = difficultyGameTypeEasy;
    [self.gm generateGamefield];
    
    /*
     UISwipeGestureRecognizer leftSwipeGesture
     */
    
    UISwipeGestureRecognizer* leftSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(leftSwipeGesture:)];
    
    leftSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:leftSwipeGesture];
    
    /*
     UISwipeGestureRecognizer rightSwipeGesture
     */
    
    UISwipeGestureRecognizer* rightSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(rightSwipeGesture:)];
    
    rightSwipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:rightSwipeGesture];
    
    /*
     UISwipeGestureRecognizer upSwipeGesture
     */
    
    UISwipeGestureRecognizer* upSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(upSwipeGesture:)];
    
    upSwipeGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:upSwipeGesture];
    
    /*
     UISwipeGestureRecognizer downSwipeGesture
     */
    
    UISwipeGestureRecognizer* downSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(downSwipeGesture:)];
    
    downSwipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:downSwipeGesture];
    
    for (UIButton* button in [self.buttonSuperview subviews]) {
        button.layer.cornerRadius = 5;
        button.layer.shadowOffset = CGSizeMake(5, 5);
        button.layer.shadowOpacity = 0.7f;
        button.layer.shadowRadius = 5;
        button.layer.shadowColor = [UIColor grayColor].CGColor;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Gestures

- (void) leftSwipeGesture: (UISwipeGestureRecognizer*) gesture {
    
    if (self.gm.rightSideButton != nil) {
        [self.gm moveButton:self.gm.rightSideButton];
    }
}

- (void) rightSwipeGesture: (UISwipeGestureRecognizer*) gesture {
    
    if (self.gm.leftSideButton != nil) {
        [self.gm moveButton:self.gm.leftSideButton];
    }
}

- (void) upSwipeGesture: (UISwipeGestureRecognizer*) gesture {
    
    if (self.gm.downSideButton != nil) {
        [self.gm moveButton:self.gm.downSideButton];
    }
}

- (void) downSwipeGesture: (UISwipeGestureRecognizer*) gesture {
    
    if (self.gm.upSideButton != nil) {
        [self.gm moveButton:self.gm.upSideButton];
    }
}

#pragma mark - GameServiceDelegate

- (void) alert:(UIAlertController *)alertController {
    [self presentViewController:alertController
                       animated:YES
                     completion:^{
                         
                     }];
}

- (void) addToView:(UIView *)view {
    [self.buttonSuperview addSubview:view];
}

#pragma mark - Actions

- (IBAction)clickAction:(UIButton *)sender {
    
    for (NSNumber* num in self.gm.availableButtonsIndex) {
        if ([num intValue] == sender.tag) {
            [self.gm moveButton:sender];
            return;
        }
    }
    
    /* animate incorrect buttons */
    UIColor* previousColor = sender.backgroundColor;
    [UIView animateWithDuration:0.2f
                     animations:^{
                         sender.backgroundColor = [UIColor redColor];
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.2f
                                          animations:^{
                                              sender.backgroundColor = previousColor;
                                          }];
                     }];
}

- (IBAction)newGameAction:(UIButton *)sender {
    
    [self.gm generateGamefield];
}

- (IBAction)difficultyControlChanged:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            self.gm.difficultGameCurrent = difficultyGameTypeEasy;
            break;
        case 1:
            self.gm.difficultGameCurrent = difficultyGameTypeMedium;
            break;
        case 2:
            self.gm.difficultGameCurrent = difficultyGameTypeHard;
            break;
        default:
            break;
    }
}

@end
