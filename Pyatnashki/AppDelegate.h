//
//  AppDelegate.h
//  Pyatnashki
//
//  Created by Tester on 3/13/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

