//
//  GameService.m
//  Pyatnashki
//
//  Created by Tester on 3/14/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "GameService.h"

@interface GameService ()

@property (assign, nonatomic) difficultyGameType difficultGame;
@property (assign, nonatomic) NSInteger indexOfEmptyButton;
@property (assign, nonatomic) NSInteger counter;
@property (assign, nonatomic) NSInteger seconds;
@property (strong, nonatomic) NSTimer* timer;


@end

@implementation GameService

- (void) searchAvailableButtons {
    
    /* clear buttons */
    self.leftSideButton = nil;
    self.rightSideButton = nil;
    self.upSideButton = nil;
    self.downSideButton = nil;
    
    NSMutableArray* indexes = [NSMutableArray array];
    
    /* UP */
    if (self.indexOfEmptyButton > 3) {
        NSInteger index = self.indexOfEmptyButton - 4;
        [indexes addObject:@(index)];
        self.upSideButton = [self.buttonsArray objectAtIndex:index];
    }
    
    /* LEFT */
    if (self.indexOfEmptyButton % 4 != 0) {
        NSInteger index = self.indexOfEmptyButton - 1;
        [indexes addObject:@(self.indexOfEmptyButton - 1)];
        self.leftSideButton = [self.buttonsArray objectAtIndex:index];
    }
    
    /* DOWN */
    if (self.indexOfEmptyButton < 12) {
        NSInteger index = self.indexOfEmptyButton + 4;
        [indexes addObject:@(self.indexOfEmptyButton + 4)];
        self.downSideButton = [self.buttonsArray objectAtIndex:index];
    }
    
    /* RIGHT */
    if (self.indexOfEmptyButton % 4 != 3) {
        NSInteger index = self.indexOfEmptyButton + 1;
        [indexes addObject:@(self.indexOfEmptyButton + 1)];
        self.rightSideButton = [self.buttonsArray objectAtIndex:index];
    }
    
    self.availableButtonsIndex = indexes;
}

- (void) checkWin {
    
    /* check win */
    for (int i = 0; i < 15; i++) {
        UIButton* currentButton = [self.buttonsArray objectAtIndex:i];
        if ((i + 1) != [[currentButton currentTitle] intValue]) {
            return;
        }
    }
    
    /* pop-up window */
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Победа"
                                 message:[NSString stringWithFormat:@"Поздравляем! Вы победили за %ld ходов!", self.counter]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    [self.timer invalidate];
    
    [self.delegate alert:alert];
}

- (void) generateGamefield {
    
    /* clear field */
    for (UIButton* button in self.buttonsArray) {
        [button setTitle:@""
                forState:UIControlStateNormal];
        button.alpha = 1.f;
    }
    self.counter = 0;
    self.counterLabel.text = @"Ходов: 0";
    self.seconds = 0;
    
    NSMutableArray* randomizeArray = [NSMutableArray array];
    
    /* create array with values between 1 and 16 */
    for (int i = 0; i < 16; i++) {
        [randomizeArray addObject:[NSNumber numberWithInt:i + 1]];
    }
    
    /* randomize array */
    for (int i = 0; i < 16; i++) {
        NSInteger random = arc4random() % 16;
        NSNumber* num = [randomizeArray objectAtIndex:i];
        [randomizeArray replaceObjectAtIndex:i
                                  withObject:[randomizeArray objectAtIndex:random]];
        
        [randomizeArray replaceObjectAtIndex:random
                                  withObject:num];
    }
    
    
    
    /* check incorrect combinations */
    NSInteger sum = 0;
    for (int i = 0; i < 16; i++) {
        NSInteger currentNumber = [[randomizeArray objectAtIndex:i] intValue];
        if (currentNumber == 16) {
            sum += (i / 4) + 1;
        }
        else {
            for (int j = (i + 1); j < 16; j++) {
                NSInteger nextNumber = [[randomizeArray objectAtIndex:j] intValue];
                if (currentNumber > nextNumber) {
                    sum++;
                }
            }
        }
    }
    if (sum % 2 == 0) {
        
        /* change button's text */
        for (UIButton* button in self.buttonsArray) {
            NSInteger index = [self.buttonsArray indexOfObject:button];
            NSInteger value = [[randomizeArray objectAtIndex:index] intValue];
            
            if (value == 16) {
                button.alpha = 0.6f;
                [button setTitle:@""
                        forState:UIControlStateNormal];
                self.indexOfEmptyButton = index;
            }
            else {
                [button setTitle:[NSString stringWithFormat:@"%ld", value]
                        forState:UIControlStateNormal];
            }
        }
        [self searchAvailableButtons];
        
        self.difficultGame = self.difficultGameCurrent;
        
        [self initTimer];
    }
    else {
        [self generateGamefield];
    }
}

- (void) updateTimer: (NSTimer*) timer {
    
    /* if difficulty is easy, then time rises */
    if (self.difficultGame == difficultyGameTypeEasy) {
        /* increment seconds and update timer's label */
        self.seconds++;
        self.timerLabel.text = [NSString stringWithFormat:@"%01ld:%02ld", self.seconds / 60, self.seconds % 60];
    }
    else {
        /* decrement seconds and update timer's label */
        self.seconds--;
        self.timerLabel.text = [NSString stringWithFormat:@"%01ld:%02ld", self.seconds / 60, self.seconds % 60];
        if (self.seconds == 0) {
            
            [self loseGame];
        }
    }
}

- (void) loseGame {
    
    [self.timer invalidate];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Проигрыш"
                                 message:@"Вы проиграли! Хотите начать новую игру или закончить эту?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Новая"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self generateGamefield];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Отмена"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   self.difficultGame = difficultyGameTypeEasy;
                                   [self initTimer];
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self.delegate alert:alert];
}

/* initialized a timer, but doesn't set self.difficultGame */
- (void) initTimer {
    
    [self.timer invalidate];
    self.timer = nil;
    
    /* initialized timer */
    /* set seconds */
    switch (self.difficultGame) {
        case difficultyGameTypeEasy:
            self.seconds = 0;
            self.timerLabel.text = @"0:00";
            break;
        case difficultyGameTypeMedium:
            self.seconds = 600;
            self.timerLabel.text = @"10:00";
            break;
        case difficultyGameTypeHard:
            self.seconds = 300;
            self.timerLabel.text = @"5:00";
            break;
        default:
            break;
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.f
                                                  target:self
                                                selector:@selector(updateTimer:)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void) moveButton: (UIButton*) sender {
    /* field under sender button */
    UIView* view = [[UIView alloc] initWithFrame:sender.frame];
    view.backgroundColor = sender.backgroundColor;
    view.alpha = 0.6f;
    view.layer.cornerRadius = 5;
    view.layer.shadowOffset = CGSizeMake(5, 5);
    view.layer.shadowOpacity = 0.7f;
    view.layer.shadowRadius = 5;
    view.layer.shadowColor = [UIColor grayColor].CGColor;
    
    UIButton* emptyButton = [self.buttonsArray objectAtIndex:self.indexOfEmptyButton];
    [self.delegate addToView:view];
    
    CGPoint previousPoint = sender.center;
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents]; // Ignoring events
    [UIView animateWithDuration:0.6f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         /* move to empty button */
                         sender.center = emptyButton.center;
                     }
                     completion:^(BOOL finished) {
                         /* back to initial point */
                         sender.center = previousPoint;
                         /* set alpha and number */
                         
                         emptyButton.alpha = 1.f;
                         [emptyButton setTitle:[sender currentTitle]
                                      forState:UIControlStateNormal];
                         
                         /* set alpha and clear */
                         sender.alpha = 0.6f;
                         [sender setTitle:@""
                                 forState:UIControlStateNormal];
                         self.indexOfEmptyButton = sender.tag;
                         [self searchAvailableButtons];
                         [self checkWin];
                         [[UIApplication sharedApplication] endIgnoringInteractionEvents]; // Stop ignoring
                         [view removeFromSuperview]; // Remove field under sender button
                     }];
    
    /* counter */
    self.counter++;
    self.counterLabel.text = [NSString stringWithFormat:@"Ходов: %ld", self.counter];
    return;
}

@end
