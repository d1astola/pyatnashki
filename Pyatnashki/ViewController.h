//
//  ViewController.h
//  Pyatnashki
//
//  Created by Tester on 3/13/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameService.h"

@interface ViewController : UIViewController <GameControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *counterLabel;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UIView *buttonSuperview;

- (IBAction)clickAction:(UIButton *)sender;
- (IBAction)newGameAction:(UIButton *)sender;
- (IBAction)difficultyControlChanged:(UISegmentedControl *)sender;

- (void) alert: (UIAlertController*) alertController;
- (void) addToView:(UIView *)view;

@end

